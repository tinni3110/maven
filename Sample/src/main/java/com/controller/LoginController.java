package com.controller;

import java.io.IOException;
import java.sql.SQLException;

import com.bean.*;
import com.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String action = request.getParameter("action");
		
		if(action.equalsIgnoreCase("Signup"))
		{
			Signup(request, response);
			
		}
	}
	
	protected void Signup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		UserBean user = new UserBean();
		user.setFirstName(request.getParameter("fname"));
		user.setLastName(request.getParameter("lname"));
		user.setEmail(request.getParameter("email"));
		user.setUserName(request.getParameter("username"));
		user.setPassword(request.getParameter("password"));
		UserService us = new UserService();
		String message="";
		
		try{
			
			us.signupService(user);
			message = "Congrats!! You have successfully signed up. Please log in to continue.";
		}catch(SQLException e){
			message = "Duplicate username. Please try again with a different one.";

		}finally {
			
			request.setAttribute("message", message);
			RequestDispatcher rd = request.getRequestDispatcher("jsp/Landing.jsp");
			rd.forward(request, response);
		}
	}

}
