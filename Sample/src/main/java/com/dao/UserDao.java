package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bean.*;
import com.util.DBUtil;


public class UserDao {
	
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs =null;
	
	public void signup(UserBean user) throws SQLException
	{
		con = DBUtil.getConnection();
		ps = con.prepareStatement("Insert into users values (?,?,?,?,?)");
		ps.setString(1, user.getUserName());
		ps.setString(2, user.getPassword());
		ps.setString(3, user.getFirstName());
		ps.setString(4, user.getLastName());
		ps.setString(5, user.getEmail());
		ps.executeQuery();
	}

}
