package com.service;

import java.sql.SQLException;

import com.bean.UserBean;
import com.dao.UserDao;

public class UserService {
	
	UserDao ud = new UserDao();
	
	public void signupService(UserBean user) throws SQLException
	{
		ud.signup(user);
	}

}
