<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.js"></script>
<script src="script.js"></script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

/* Set a style for all buttons */


button:hover {
  opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

/* Center the image and position the close button */
.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
  position: relative;
}

img.avatar {
  width: 40%;
  border-radius: 50%;
}

.xyz {
  padding: 20px;
}

span.psw {
  float: right;
  padding-top: 16px;
}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
  border: 1px solid #888;
  width: 38%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
  position: absolute;
  right: 25px;
  top: 0;
  color: #000;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: red;
  cursor: pointer;
}

/* Add Zoom Animation */
.animate {
  -webkit-animation: animatezoom 0.6s;
  animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
  from {-webkit-transform: scale(0)} 
  to {-webkit-transform: scale(1)}
}
  
@keyframes animatezoom {
  from {transform: scale(0)} 
  to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>
</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
		    <div class="navbar-header">
		     	<a class="navbar-brand" >Sanjana</a>
		    </div>
		    <ul class="nav navbar-nav nav-pills">
    			<li class="active"><a data-toggle="pill" href="#home">Home</a></li>
    			<li><a data-toggle="pill" href="#About">About</a></li>
    			<li><a data-toggle="pill" href="#Services">Services</a></li>
    			<li><a data-toggle="pill" href="#Contact">Contact</a></li>
  			</ul>
  			<ul class="nav navbar-nav navbar-right">
		    	<li><a data-toggle="pill" onclick="document.getElementById('Signup').style.display='block'"><span class="glyphicon glyphicon-user"></span>Sign Up</a></li>
		        <li><a data-toggle="pill" href="#Login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
		    </ul>
	  	</div>
	</nav>
	<div class="tab-content">
	    <div id="home" class="tab-pane fade in active">
	      <h3>Welcome</h3>
	      <p>Test Home</p>
	    </div>
	    <div id="About" class="tab-pane fade">
	      <h3>About</h3>
	      <p>Test About</p>
	    </div>
	    <div id="Services" class="tab-pane fade">
	      <h3>Services</h3>
	      <p>Test Services</p>
	    </div>
	    <div id="Contact" class="tab-pane fade">
	      <h3>Contact Us</h3>
	      <p>Test Contact</p>
	    </div>
	    <div id="Signup" class="modal">
	      <form class="modal-content animate" name="myForm" action="LoginController" method="post">
	      		<div class="imgcontainer">
			      <span onclick="document.getElementById('Signup').style.display='none'" class="close" title="Close Modal">&times;</span>
			      <h1 style="color:green;">SIGN UP</h1>
			    </div>
	      	
	      		<div class="xyz">
					<input type="text" placeholder="Enter First Name" id="fname" name ="fname" class="form-control" required>

					<input type="text" placeholder="Enter Last Name" id="lname" name ="lname" class="form-control"  required>
												
      				<input type="email" placeholder="Enter Email" name="email" id="email" class="form-control"  required>
      				
      				<input type="text" placeholder="Enter Username" name="username" id="username" class="form-control"  required>
	
					<input type="password" placeholder="Enter Password" id="password" name="password" class="form-control"  required>
	
					<input type="password" placeholder="Re-enter your password" id="cpassword" name="cpassword" class="form-control" required>
					
					<button type="submit" class="btn btn-success col-sm-12" name="action" value="Signup" style="width: :100%;margin-top: 10px;">Submit</button>
				</div>	
				
			    <div class="xyz" style="background-color:#f1f1f1; margin-top: 40px;">
			      <button type="button" onclick="document.getElementById('Signup').style.display='none'" class="btn btn-danger">Cancel</button>
			      <span class="psw">Forgot <a href="#">password?</a></span>
			    </div>
	      </form>
	    </div>
	    <div id="Login" class="tab-pane fade">
	      <h3>Login</h3>
	      <p>Test Contact</p>
	    </div>
    </div>

<script>
// Get the modal
var modal = document.getElementById('Signup');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
</body>
</html>