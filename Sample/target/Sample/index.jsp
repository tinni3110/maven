<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
</head>

<body>
<div class="container">
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
		    <div class="navbar-header">
		     	<a class="navbar-brand" >MY APP</a>
		    </div>
		    <ul class="nav navbar-nav nav-pills">
    			<li class="active"><a data-toggle="pill" href="#home">Home</a></li>
    			<li><a data-toggle="pill" href="#About">About</a></li>
    			<li><a data-toggle="pill" href="#Services">Services</a></li>
    			<li><a data-toggle="pill" href="#Contact">Contact</a></li>
  			</ul>
  			<ul class="nav navbar-nav navbar-right">
		    	<li><a data-toggle="pill" href="#Signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
		        <li><a data-toggle="pill" href="#Login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
		    </ul>
	  	</div>
	</nav>
	<div class="tab-content">
	    <div id="home" class="tab-pane fade in active">
	      <h3>Welcome</h3>
	      <p>Test Home</p>
	    </div>
	    <div id="About" class="tab-pane fade">
	      <h3>About</h3>
	      <p>Test About</p>
	    </div>
	    <div id="Services" class="tab-pane fade">
	      <h3>Services</h3>
	      <p>Test Services</p>
	    </div>
	    <div id="Contact" class="tab-pane fade">
	      <h3>Contact Us</h3>
	      <p>Test Contact</p>
	    </div>
	    <div id="Signup" class="tab-pane fade">
	      <form name="myForm" action="LoginController" method="post">
	      	<div class="form-group">
			    <label for="name">NAME:</label>
			    <input type="text" class="form-control" id="name" name ="name">
			</div>
			<div class="form-group">
			    <label for="email">Email address:</label>
			    <input type="email" class="form-control" id="email" name ="email">
			</div>
			<div class="form-group">
			    <label for="password">Password:</label>
			    <input type="text" class="form-control" id="password" name="password">
			</div>
			<button type="submit" class="btn btn-default" name="action" value="login">Submit</button>
	      </form>
	    </div>
	    <div id="Login" class="tab-pane fade">
	      <h3>Login</h3>
	      <p>Test Contact</p>
	    </div>
    </div>
</div>
</body>
</html>